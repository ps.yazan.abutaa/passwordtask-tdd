package com.progressoft.tddpractice.passwordtask_tdd;

import com.progressoft.tddpractice.passwordtask_tdd.PasswordGenerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordGeneratorTest
{

    private PasswordGenerator passwordGenerator;

    @BeforeEach
    public void beforeEach()
    {
        passwordGenerator = new PasswordGenerator();
    }

    @RepeatedTest(100)
    public void givenPasswordGenerator_whenGeneratePassword_thenReturnValidPassword()
    {
        //Password not null
        //Password of length 8
        //Password only allows A-Z, 0-9, %$#_
        //Password only allows 2 UNIQUE letters (a!=b)
        //Password only allows 4 UNIQUE numbers (a!=b!=c!=d)
        //Password only allows 2 UNIQUE symbols (a!=b) ABCD12$#
        //Password must be shuffled

        String generatedPassword = passwordGenerator.generate();

        Assertions.assertNotNull(generatedPassword, "Password generated is null");
        Assertions.assertEquals(generatedPassword.length(), 8, "Password length is invalid");
        Assertions.assertFalse(hasIllegalCharacters(generatedPassword), "Password contains illegal characters");
        Assertions.assertTrue(isShuffled(generatedPassword),"Password "+ generatedPassword + " is not shuffled");
    }

    @RepeatedTest(100)
    public void givenPasswordGenerator_whenGeneratePassword_thenReturnPasswordWithFourUppercaseLetters()
    {
        String generatedPassword = passwordGenerator.generate();
        int count = countRegexMatches("[A-Z]",generatedPassword);
        Assertions.assertEquals(count, 2, "Password: " + generatedPassword + ", found " + count + " uppercase letters, where there only has to be 4.");
    }

    @RepeatedTest(100)
    public void givenPasswordGenerator_whenGeneratePassword_thenReturnPasswordWithTwoNumbers()
    {
        String generatedPassword = passwordGenerator.generate();
        int count = countRegexMatches("[0-9]",generatedPassword);
        Assertions.assertEquals(count, 4, "Password: " + generatedPassword + ", found " + count + " numbers, where there only has to be 2.");
    }

    @RepeatedTest(100)
    public void givenPasswordGenerator_whenGeneratePassword_thenReturnPasswordWithTwoSymbols()
    {
        String generatedPassword = passwordGenerator.generate();
        int count = countRegexMatches("[#$%_]",generatedPassword);
        Assertions.assertEquals(count, 2, "Password: " + generatedPassword + ", found " + count + " numbers, where there only has to be 2.");
    }

    @RepeatedTest(100)
    public void givenPasswordGenerator_whenGeneratePassword_thenReturnAllUniqueCharacters()
    {
        String generatedPassword = passwordGenerator.generate();
        HashSet hashSet = new HashSet<Character>();

        for(int i = 0; i < generatedPassword.length();i++)
            Assertions.assertTrue(hashSet.add(generatedPassword.toCharArray()[i]),"Password: " + generatedPassword + " contains non-unique characters");
    }

    private boolean hasIllegalCharacters(String password)
    {
        return !(Pattern.compile("^([A-Z0-9#$%_])+$").matcher(password).matches());
    }

    private int countRegexMatches(String regex, String string)
    {
        Matcher passwordMatcher = Pattern.compile(regex).matcher(string);

        int count = 0;
        while(passwordMatcher.find())
            count++;

        return count;
    }

    private boolean isShuffled(String string)
    {
        //Make sure no chars of the same group end up next to each other
        boolean uppercaseShuffled = Pattern.compile("[0-9]{4}").matcher(string).find();
        boolean numbersShuffled   = Pattern.compile("[A-Z]{2}").matcher(string).find();
        boolean symbolsShuffled   = Pattern.compile("[#$%_]{2}").matcher(string).find();
        return !(uppercaseShuffled && numbersShuffled && symbolsShuffled);
    }

    @Test
    public void givenPasswordGenerator_whenConstructor_thenReturnProperly()
    {
        Assertions.assertNotNull(passwordGenerator, "PasswordGenerator is null");
    }
}