package com.progressoft.tddpractice.passwordtask_tdd;

import java.util.Random;

public class PasswordGenerator {

    private int passwordLength;
    private Random random;


    public PasswordGenerator() {
        passwordLength = 8;
        random = new Random();
    }

    public String generate() {
        PasswordSet.reset();
        return generatePassword();
    }

    private String generatePassword() {

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < passwordLength; i++) {
            int setIndex = random.nextInt(PasswordSet.values().length);

            if (PasswordSet.values()[setIndex].count() > 0)
                stringBuilder.append(PasswordSet.values()[setIndex].select());
            else i--;
        }


        return stringBuilder.toString();
    }
}

